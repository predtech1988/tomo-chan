# Tomo-chan
Productivity time tracker app. Based on [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hrh9aRCrtqs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Table of Contents
1. [Installation](#installation)
2. [Third parties](#third-parties)

## Installation
1. Go to https://godotengine.org/download
2. Download and install 3.x version (i am using 3.4.2)
3. clone repo `git clone https://gitlab.com/predtech1988/tomo-chan.git`
4. Start Godot engine
5. At the right side press "Import" or press Ctr+I
6. Locate cloned folder press "Import and Edit"
7. Project will appear in "Local Projects" tab
8. Double click project name
9. After project loaded press F5 to start
10. Install Godot-SQLite 
```
    Click on the 'AssetLib' button at the top of the editor.
    Search for 'godot-sqlite' and click on the resulting element.
    In the dialog pop-up, click 'Download'.
    Once the download is complete, click on the install button...
    Once more, click on the 'Install' button.
    Activate the plugin in the 'Project Settings/Plugins'-menu.
    All done!
```

## Third parties
```
Engine: Godot https://godotengine.org
Background: Алексей "FROdO" Сумкин https://vk.com/frodo_1
Icons: made by Freepik https://www.freepik.com
Graphic editor: GIMP https://www.gimp.org/
Font: FiraCode https://github.com/tonsky/FiraCode
Sounds: https://freesound.org/
	by JohnsonBrandEditing
	by ConBlast
```