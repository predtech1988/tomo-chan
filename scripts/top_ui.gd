extends Node
# Show/Hide shadow when mouse hover over top_ui elements

func show_hide_shadow(btn_name):
	var node = get_node(btn_name+"/shadow")
	node.visible = !(node.visible)	


# Settings icon
func _on_settings_mouse_entered():
	show_hide_shadow("settings")


func _on_settings_mouse_exited():
	show_hide_shadow("settings")	


# Stats icon
func _on_stats_mouse_entered():
	show_hide_shadow("stats")


func _on_stats_mouse_exited():
	show_hide_shadow("stats")


# Mode icon
func _on_mode_mouse_entered():
	show_hide_shadow("mode")


func _on_mode_mouse_exited():
	show_hide_shadow("mode")


# Tomatos icon
func _on_tomatos_mouse_entered():
	show_hide_shadow("tomatos")


func _on_tomatos_mouse_exited():
	show_hide_shadow("tomatos")


###### Buttons ######
func _on_settings_pressed():
	Global.get_all_user_propertys()
