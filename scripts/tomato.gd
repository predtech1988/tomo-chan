extends Node

var SetTimerWindow = preload("res://scenes/timer_selector.tscn")

# Shades  buttons when mouse hover
func _on_btn_play_mouse_entered():
	self.get_node("btn_play").set_modulate(Color(0.59, 0.59, 0.59, 1))


func _on_btn_pause_mouse_entered():
	self.get_node("btn_pause").set_modulate(Color(0.59, 0.59, 0.59, 1))
	
	
func _on_btn_stop_mouse_entered():
	self.get_node("btn_stop").set_modulate(Color(0.59, 0.59, 0.59, 1))
	
	
func _on_btn_timer_screen_mouse_entered():
	# If timer started, we can't change it value
	# so we disable here hover
	if not(Global.is_start):
		self.get_node("timer_screen/btn_timer_screen/shadow").visible = true


# Brings back button color
func _on_btn_play_mouse_exited():
	self.get_node("btn_play").set_modulate(Color(1, 1, 1, 1))


func _on_btn_pause_mouse_exited():
	self.get_node("btn_pause").set_modulate(Color(1, 1, 1, 1))
	
	
func _on_btn_stop_mouse_exited():
	self.get_node("btn_stop").set_modulate(Color(1, 1, 1, 1))


func _on_btn_timer_screen_mouse_exited():
	self.get_node("timer_screen/btn_timer_screen/shadow").visible = false


# 'Start timer' button logic
func _on_btn_play_pressed():
	$btn_play.disabled = true
	$btn_play.visible = false
	$timer_screen/btn_timer_screen.disabled = true
	$btn_pause.visible = true
	$btn_pause.disabled = false
	$btn_stop.disabled = true
	Global.is_start = true


# 'Pause timer' button logic
func _on_btn_pause_pressed():
	$btn_pause.disabled = true
	$btn_pause.visible = false
	$btn_play.disabled = false
	$btn_play.visible = true
	$btn_stop.disabled = false
	Global.is_start = false
	$timer_screen/timer_separator.visible = true


# 'Stop timer' button logic
func _on_btn_stop_pressed():
	$btn_pause.disabled = true
	$btn_pause.visible = false
	$btn_play.visible = true
	$btn_play.disabled = false
	Global.is_start = false
	Global.reset_timer()
	$timer_screen.update_timer_screen()
	$timer_screen/timer_separator.visible = true
	$timer_screen/btn_timer_screen.disabled = false


######		Logic		#####

func count_end() -> void:
	$btn_play.set_modulate(Color(1, 1, 1, 1))
	_on_btn_stop_pressed()
	$"/root/main".count_ended()
	
	if not Global.resting:
		Global.resting = true
		print_debug(("working"))
	else:
		print_debug("resting")
		Global.resting = false
	
	Global.reset_timer()
	$timer_screen.update_timer_screen()


func _on_btn_timer_screen_pressed() -> void:
	"""
	When user press Timer Screen Button to set time, we disable 
	all buttons on the main screen, and disabling hoover for them.
	"""
	if Global._settings["mode"] == "pomadoro":
		# In classic mode we can't set timer
		return
	# Getting all buttons that in the "main_screen_buttons" group
	var buttons = get_tree().get_nodes_in_group("main_screen_buttons")
	for button in buttons:
		button.disabled = true
		button.set_mouse_filter(2) # 0 - Default 'Stop', 2 - 'Ignore'
	var set_timer_window = SetTimerWindow.instance()
	self.get_node(".").add_child(set_timer_window)
