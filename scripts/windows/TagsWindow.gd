extends Node

var WindowMan = load("res://scripts/main.gd").new()


func _on_btn_close_mouse_entered():
	$Buttons/btn_close/btn_bg.color = (Color(1, 1, 1, 0.1))


func _on_btn_close_mouse_exited():
	$Buttons/btn_close/btn_bg.color = (Color(0, 0, 0, 0.25))


func _on_btn_close_pressed():
	WindowMan.disable_enable_all_buttons(0, self)
	self.queue_free()


func _on_TagsWindow_tree_entered():
	WindowMan.disable_enable_all_buttons(2, self)


func _on_TagsWindow_tree_exited():
	pass
