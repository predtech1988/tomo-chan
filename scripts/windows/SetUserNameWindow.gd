extends Node

func _on_btn_set_mouse_entered():
	$Buttons/btn_set.set_modulate(Color(0.59, 0.59, 0.59, 1))


func _on_btn_set_mouse_exited():
	$Buttons/btn_set.set_modulate(Color(1, 1, 1, 1))


func _on_btn_cancel_mouse_entered():
	$Buttons/btn_cancel.set_modulate(Color(0.59, 0.59, 0.59, 1))


func _on_btn_cancel_mouse_exited():
	$Buttons/btn_cancel.set_modulate(Color(1, 1, 1, 1))


func _on_btn_set_pressed():
	var user_name = $Control/user_name.text
	if len(user_name):
		Global.change_setting("user_name", user_name)
		Global.change_setting("first_run", false)
		_on_btn_cancel_pressed()


func _on_btn_cancel_pressed():
	var root = get_tree().get_current_scene()
	root.disable_enable_all_buttons(0)
	self.queue_free()
