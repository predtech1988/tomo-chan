extends Node
var Tag = load("res://scenes/elements/tag.tscn")
var DB = load("res://scripts/db.gd").new()
var WindowMan = load("res://scripts/main.gd").new()
var caller = ""


func _on_btn_close_mouse_entered():
	$Buttons/btn_close/btn_bg.color = (Color(1, 1, 1, 0.1))


func _on_btn_close_mouse_exited():
	$Buttons/btn_close/btn_bg.color = (Color(0, 0, 0, 0.25))


func _on_btn_close_pressed():
	WindowMan.disable_enable_all_buttons(0, self)
	self.queue_free()


func _on_QuickTag_tree_entered():
	WindowMan.disable_enable_all_buttons(2, self)
	update_tag_list()


func update_tag_list():
	var container = $Control/tags_list/tags_container/VBoxContainer
	var tags = DB.get_tags()
	for t in tags:
		if t["tag_name"] in Global._active_tags.values():
			continue
		var tag = Tag.instance()
		tag.caller = caller
		tag.get_node("Node2D/Label").text = t["tag_name"]
		container.add_child(tag)
	# Add last tag 
	# (Magic ^_^ if t["score"] = 0 last tag not shown) 
	# if max_tags >= 10 last tag appears twice 
	# Need more investigation later TODO
	#if len(tags) > 10 and not(tags[-1]["tag_name"] in Global._active_tags.values()):
	if not(tags[-1]["tag_name"] in Global._active_tags.values()):
		var tag = Tag.instance()
		tag.get_node("Node2D/Label").text = tags[-1]["tag_name"]
		container.add_child(tag)
