# Manage tags Buttons on the Main screen
extends Node

var QuickTag = load("res://scenes/quick_tag.tscn")
var TagsWindow = load("res://scenes/tags.tscn")


func on_tag_x_pressed(btn:String) -> void:
	# If tag Button hase text "..." open QuickTag Window
	# If tag have text "tag name" change text on the Button, and clear tag list
	var btn_name = "tag_%s/lab_%s" % [btn, btn]
	var text = self.get_node(btn_name).text
	var root = get_tree().get_current_scene()

	if text == "...":
		var quick_tag = QuickTag.instance()
		quick_tag.caller = btn
		root.add_child(quick_tag)
		return
	Global._active_tags[str(int(btn)-1)] = "..."
	update_tags_text(self)
	return


func update_tags_text(obj) -> void:	
	var lables = obj.get_tree().get_nodes_in_group("tags")
	for i in range(0, 4):
		if lables[i].text == "...":
			lables[i].text = Global._active_tags[str(i)]
		else:
			lables[i].text = Global._active_tags[str(i)]


func clear_tags() -> void:
	"""
	Clearing all tags from the Global._active_tags Dict
	First we clean Tag Buttons from the Main Window
	Than remove rest of the tag from the Global._active_tags Dict
	"""
	for i in ["0", "1", "2", "3"]:
		Global._active_tags[i] = "..."

	# Removing rest of the tags
	for key in Global._active_tags.keys():
		if not(key in ["0", "1", "2", "3"]):
			Global._active_tags.erase(key)


func _on_tag_1_pressed() -> void:
	on_tag_x_pressed("1")


func _on_tag_2_pressed() -> void:
	on_tag_x_pressed("2")


func _on_tag_3_pressed() -> void:
	on_tag_x_pressed("3")


func _on_tag_4_pressed() -> void:
	on_tag_x_pressed("4")


func _on_tag_add_more_pressed():
	var root = get_tree().get_current_scene()
	var tags_window = TagsWindow.instance()
	root.add_child(tags_window)
