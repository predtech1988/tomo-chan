extends Node

var Numbers = preload("res://scenes/numbers.tscn")


func fill_numbers():
	"""
	Filling up numbers slots in Set timer window
	"""
	var vb_hours = $hours/ScrollContainer/VBoxContainer
	var vb_minutes = $minutes/ScrollContainer/VBoxContainer
	var vb_seconds = $seconds/ScrollContainer/VBoxContainer

	for i in range(66):
		var val = ""
		# Adding leading zeros
		if (i <= 9):
			val = "0{0}".format({0: str(i)})
		else:
			val = str(i)

		# Adding hours, minutes, seconds to the Labels
		var num = Numbers.instance()
		if i <=29:	
			num.get_node("Panel/Node2D/Label").text = val
			vb_hours.add_child(num)
		if i > 59:
			val = ""
		num = Numbers.instance()
		num.get_node("Panel/Node2D/Label").text = val
		vb_minutes.add_child(num)
		
		num = Numbers.instance()
		num.get_node("Panel/Node2D/Label").text = val
		vb_seconds.add_child(num)


func _ready():
	fill_numbers()


var hours = 0
var minutes = 0
var seconds = 0


func get_number_from_set_timer(a:Area2D) -> int:
	return int(a.get_owner().get_node("Panel/Node2D/Label").text)


func get_specified_time() -> Array:
	return [hours, minutes, seconds]


func set_numbers_color(area:Area2D, color:Color) -> void:
	area.get_owner().get_node("Panel/Node2D/Label").set_modulate(color)


func set_specified_time(time:Array) -> int:
	"""
	Calculating user input to the seconds. 
	We receive array in format [hours:int, minutes:int, seconds:int], and then 
	covert all data in to the seconds and save it in the Global 'time_left' property.
	"""
	var _minutes = (time[0] * 60) + time[1]
	var _seconds = (_minutes * 60) + time[2]
	Global.time_left = _seconds
	Global._cur_session_time = _seconds
	return _seconds


# Signals
func _on_hours_area_entered(area):
	hours = get_number_from_set_timer(area)
	set_numbers_color(area, Color(0.11, 0.85, 0.82, 1))


func _on_minutes_area_entered(area):
	minutes = get_number_from_set_timer(area)
	set_numbers_color(area, Color(0.11, 0.85, 0.82, 1))

func _on_seconds_area_entered(area):
	seconds = get_number_from_set_timer(area)
	set_numbers_color(area, Color(0.11, 0.85, 0.82, 1))

func _on_hours_area_exited(area):
	set_numbers_color(area, Color(1, 1, 1, 1))


func _on_minutes_area_exited(area):
	set_numbers_color(area, Color(1, 1, 1, 1))


func _on_seconds_area_exited(area):
	set_numbers_color(area, Color(1, 1, 1, 1))


func _on_btn_set_mouse_entered():
	self.get_owner().get_node("Buttons/btn_set").set_modulate(Color(0.59, 0.59, 0.59, 1))


func _on_btn_set_mouse_exited():
	self.get_owner().get_node("Buttons/btn_set").set_modulate(Color(1, 1, 1, 1))


func _on_btn_cancel_mouse_entered():
	self.get_owner().get_node("Buttons/btn_cancel").set_modulate(Color(0.59, 0.59, 0.59, 1))


func _on_btn_cancel_mouse_exited():
	self.get_owner().get_node("Buttons/btn_cancel").set_modulate(Color(1, 1, 1, 1))


############	Buttons logic	############
func _on_btn_cancel_pressed():
	self.get_owner().queue_free() # Removes 'Set Time' window from main screen
	var buttons = get_tree().get_nodes_in_group("main_screen_buttons")
	for button in buttons:
		button.disabled = false
		button.set_mouse_filter(0) # 0 - Default 'Stop', 2 - 'Ignore'


func _on_btn_set_pressed():
	var _seconds = set_specified_time(get_specified_time())
	var root = get_tree().get_current_scene()
	root.get_node("tomato_timer/timer_screen").update_timer_screen()
	if Global._settings["mode"] == "custom":
		Global.change_setting("time_left", _seconds)
	_on_btn_cancel_pressed() # Remove 'Set time' window
