extends Node

const SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var db =SQLite.new()
var db_name := "user://db/tomo-chan"


func create_new_db() -> void:
	# Copying new clean DB
	var dir = Directory.new()
	dir.open("user://")
	dir.make_dir("db")
	dir.copy("res://assets/misc/tomo-chan.db", "user://db/tomo-chan.db")


func get_tags() -> Array:
	var db = SQLite.new()
	db.path = db_name
	db.verbose_mode = false
	db.open_db()
	db.query_with_bindings("""
		SELECT * 
		FROM tags
		ORDER BY score DESC
		LIMIT ?
	""", [Global._settings.max_tags])
	db.close_db()
	return db.query_result


func get_tag_by_name(tag_name:String) -> Array:
	"""
	Returns tag_id with given tag_name
	"""
	var db = SQLite.new()
	db.path = db_name
	db.open_db()
	db.query_with_bindings("""
		SELECT tag_id 
		FROM tags
		WHERE tag_name = ?
	""", [tag_name])
	db.close_db()
	return db.query_result


func save_tomato(tomato:Dictionary) -> void:
	var db = SQLite.new()
	db.path = db_name
	db.open_db()
	db.insert_row("sessions", tomato)
	db.close_db()


func update_tag_score(tag_id:String) -> void:
	var select_condition: String = "tag_id = %s" % [tag_id]
	var db = SQLite.new()
	db.path = db_name
	db.open_db()
	var selected_array: Array = db.select_rows("tags", select_condition, ["*"])
	var current_score: int = selected_array[0]["score"]
	selected_array[0]["score"] = current_score + 1
	db.update_rows("tags", select_condition,selected_array[0])
	db.close_db()


func get_classic_tomatos_count(date:String) -> int:
	var db = SQLite.new()
	db.path = db_name
	db.open_db()
	db.query_with_bindings("""
		SELECT count(session_id) AS tomatos
		FROM sessions
		WHERE session_date LIKE ?;
	""", [date])
	db.close_db()
	# If query returns NULL return 0
	if typeof(db.query_result[0]["tomatos"]) == 0:
		return 0
	else:
		return db.query_result[0]["tomatos"]


func get_custom_tomatos_count(date:String) -> int:
	var db = SQLite.new()
	db.path = db_name
	db.open_db()
	# Getting total seconds of the today's session and 
	# convert to the minutes, and divide by "custom_tomato_time"
	db.query_with_bindings("""
		SELECT ROUND(((SUM(time_took) / 60.0)/ ?), -1) AS tomatos
		FROM sessions
		WHERE session_date LIKE ?;
	""", [Global._settings["custom_tomato_time"], date])
	db.close_db()
	# If query returns NULL return 0
	if typeof(db.query_result[0]["tomatos"]) == 0:
		return 0
	else:
		return db.query_result[0]["tomatos"]
