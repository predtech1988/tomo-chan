extends Node

var DB = preload("res://scripts/db.gd").new()


func get_selected_tags_ids() -> String:
	"""
	Not efficient, need to rework  and get id at the first query.
	Returns string with comma separated tag id's 
	"""
	var tags:= PoolStringArray()
	for tag in Global._active_tags.values():
		if tag == "...":
			continue
		else:
			var cur_tag = DB.get_tag_by_name(tag)
			tags.append(cur_tag[0]["tag_id"])
	return tags.join(", ")


func format_date(utc:bool=true) -> String:
	"""
	Formats current date in postgress timestamp format
	'YYYY-MM-DD HH:MI:SS'
	"""
	var current_date: Dictionary = OS.get_datetime(utc)
	var date: String = '%s-%s-%s %s:%s:%s' % [current_date["year"], 
	current_date["month"], current_date["day"],
	current_date["hour"], current_date["minute"],
	current_date["second"]
	 ]
	return date


func add_tomato() -> void:
	"""
	Add new tomato to the DB
	Tomato saved as session in format:
	(date:timestamp , user:int, time_took:int,   tags:str)
	(2022-2-11 15:28:45,   1,    3600,    '1, 4, 5')
	"""
	var tags_ids: String = get_selected_tags_ids()
	var tomato: Dictionary = {
		"session_date": format_date(),
		"user_id": Global._settings["user_id"],
		"time_took": Global._cur_session_time,
		"tags": tags_ids
	}
	DB.save_tomato(tomato)
	if tags_ids.length() > 0:
		recalculate_tag_score(tags_ids)


func recalculate_tag_score(tags_ids:String) -> void:
	var tags: PoolStringArray = tags_ids.split(", ")
	for tag in tags:
		DB.update_tag_score(tag)


func count_tomatos() -> int:
	#Get only YYYY-MM-DD part utc=true
	var date: String = format_date(true).split(" ")[0]
	var tomatos_count: int
	
	if Global._settings["mode"] == "pomadoro":
		tomatos_count = DB.get_classic_tomatos_count(date + " %")
	else:
		tomatos_count = DB.get_custom_tomatos_count(date + " %")
	return tomatos_count


func is_short_break() -> bool:
	"""
	Get todays sessions count. 
	If a multiple of 4 -> lobg_break return false
	If not a multiple of 4 -> short_break return true	
	"""
	var date: String = format_date(true).split(" ")[0]
	var tomatos_count: int = DB.get_classic_tomatos_count(date + " %")
	return bool(tomatos_count%4)


func set_break_length() -> void:
	if is_short_break():
		Global._is_short_break = true
	else:
		Global._is_short_break = false
