extends Node
var DB = preload("res://scripts/db.gd").new()

var _settings = load_settings()
var _TIME_LEFT = 1800
var time_left = _settings.get("time_left")  	# Time left to count
var is_start = false     	# Timer stopped
var _cur_session_time
var resting = false
var _is_short_break = true
var _selected_tags = {} 	# All user selected tags

# Tags on Main Window tags Buttons
var _active_tags = {
	"0": "...",
	"1": "...",
	"2": "...",
	"3": "...",
}   	


func copy_default_settings() -> void:
	# Copying default settings file to the user directory, reset settings.
	var defaultsettings = File.new()
	defaultsettings.open("res://settings.json", File.READ)
	var data = defaultsettings.get_as_text()
	var new_user_settings = File.new()
	new_user_settings.open("user://settings.json", File.WRITE)
	new_user_settings.store_string(data)
	defaultsettings.close()
	new_user_settings.close()


func reset_timer() -> void:
	if resting:
		if _settings.get("mode") == "pomadoro":
			Global.time_left = 300 if _is_short_break else 900
		else:
			Global.time_left = _settings.get("short_break") if _is_short_break else _settings.get("long_break")
		return
	
	if _settings.get("mode") == "pomadoro":
		Global.time_left = 1500	# Default pomadoro time 25min
	elif _settings.get("mode") == "custom":
		Global.time_left = _settings.get("time_left")
	else:
		Global.time_left = _TIME_LEFT


func get_all_user_propertys() -> Dictionary:
	var props = get_script().get_script_property_list() 	# Getting current script property's
	var data = _settings
	for prop in props:
		if prop.name[0] == "_": 	# Skip "privat" property's
			continue
		data[prop.name] = get(prop.name)
	save_settings(data)
	return data


func save_settings(data_dict:Dictionary) -> void:
	var data = var2str(data_dict)
	var file = File.new()
	file.open("user://settings.json", File.WRITE)
	file.store_string(data)
	file.close()


func load_settings():
	var is_first_run = not(File.new().file_exists("user://settings.json"))
	if is_first_run:
		copy_default_settings()
	var file = File.new()
	file.open("user://settings.json", File.READ)
	var data = file.get_as_text()
	file.close()
	return str2var(data)


func change_setting(key:String, val) -> void:
	_settings[key] = val
	save_settings(_settings)


func _ready():
	if _settings.get("first_run"):
		var root = get_tree().get_current_scene()
		root.set_user_name()
		DB.create_new_db()
