extends Node


# 'Blinkin' separator every second and countdown
func _on_blink_timeout():
	if Global.is_start:
		var node = $timer_separator
		node.visible = !(node.visible)
		update_timer_screen()
		Global.time_left -= 1


func blink_status(status:bool):
	Global.is_start = true if status else  false


func _process(delta):
	if Global.time_left == 0 and Global.is_start:
		# Call stop button method
		var root = get_tree().get_current_scene().get_node("tomato_timer")
		root.call("count_end")


# Formated time H:M:S
var formated_time = {
	"h": "0",
	"m": "0",
	"s": "0"
}


func format_time(time: int) -> Dictionary:
	"""
	Receive time in Seconds, and convert in human readable format H : M : S
	:Parameters:
		- `time` (`int`) - seconds left until timer go to zero
	:return: (`Dictionary`) Dictionary with converted time. "key:str":"val:str"
	"""
	var minutes 
	
	if time > 3600:
		# We have 1 Hour or more until coundown ends
		var _hour = (time / 60) /60 
		minutes = (time - (_hour * 60 * 60)) / 60
		# Add leading 0 if hours or minutes  less than 10
		if _hour < 10:
			_hour = "0" + str(_hour)
		if minutes < 10:
			minutes	= "0" + str(minutes)
		formated_time["h"] = str(_hour)
		formated_time["m"] = str(minutes)
		return formated_time
	
	else:
		# We have less than 1 Hour 
		minutes = time / 60
		var seconds = time % 60
		# Add leading 0 if minutes or seconds less than 10
		if seconds < 10:
			seconds = "0" + str(seconds)
		if minutes < 10:
			minutes	= "0" + str(minutes)
		formated_time["m"] = str(minutes)
		formated_time["s"] = str(seconds)
		return formated_time


func update_timer_screen(f_time=format_time(Global.time_left)):
	if Global.time_left <= 3600:
		$timer_digits.text = "{m} {s}".format(f_time)
	else:
		$timer_digits.text = "{h} {m}".format(f_time)


func _on_timer_digits_ready():
	Global.reset_timer()
	Global._cur_session_time = Global.time_left
	update_timer_screen()
