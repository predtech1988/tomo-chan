extends Node

func play_sound(song_name:String) -> void:
	var played = false
	song_name = "res://assets/sounds/%s" % song_name
	$AudioStreamPlayer2D.stream = load(song_name)
	$AudioStreamPlayer2D.play()


func _on_AudioStreamPlayer2D_finished():
	self.queue_free()
