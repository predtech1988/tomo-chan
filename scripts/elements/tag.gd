extends Node

var MainWinTagsBtn = load("res://scripts/main_wind_tags_btns.gd").new()
var QuickTag =load("res://scripts/windows/QuickTag.gd").new()
var WindowMan = load("res://scripts/main.gd").new()
var caller = ""


func _on_btn_pressed():
	"""
	Getting tag Button id, saving tag "text" in the _active_tags Dict 
	{"btn_id:str" : "text:str"}
	and closing QuickTag selection Window.
	"""
	var tag_btn_id = int(self.caller) -1
	var root = get_tree().get_current_scene()	
	Global._active_tags[str(tag_btn_id)] = $Node2D/Label.text
	MainWinTagsBtn.update_tags_text(self)
	WindowMan.disable_enable_all_buttons(0, self)
	root.get_node('QuickTag').queue_free()


func _on_btn_mouse_entered():
	$Node2D/Panel.set_modulate(Color(0, 0, 0, 1))


func _on_btn_mouse_exited():
	$Node2D/Panel.set_modulate(Color(0.2, 1, 0.95, 1))
