extends Node

var AudioPlayer = load("res://scenes/audio_player.tscn")
var SetUserNameWindow = preload("res://scenes/set_user_name.tscn")
var TomatoLogic = load("res://scripts/tomato_logic.gd").new()
var MainWindTags = load("res://scripts/main_wind_tags_btns.gd").new()


func _ready():
	update_tomatos_counter()


func play_snd():
	var audio_player = AudioPlayer.instance()
	self.add_child(audio_player)
	audio_player.play_sound(Global._settings.short_ding)


func count_ended():
	#TomatoLogic
	TomatoLogic.set_break_length()	
	# If resting cycle don't add tomato
	
	if not Global.resting:
		TomatoLogic.add_tomato()
		MainWindTags.clear_tags()
		MainWindTags.update_tags_text(self)
	play_snd()


func disable_enable_all_buttons(state:int, obj=self) -> void:
	"""
	Disable/Enable all buttons on the main screen.
	state:int 0-Enable, 2-Disable hover above buttons
	"""
	var buttons = obj.get_tree().get_nodes_in_group("main_screen_buttons")
	for button in buttons:
		button.disabled = button.disabled
		button.set_mouse_filter(state) # 0 - Default 'Stop', 2 - 'Ignore'


func set_user_name() -> void:
	disable_enable_all_buttons(2)
	var set_user_name_window = SetUserNameWindow.instance()
	self.add_child(set_user_name_window)


func update_tomatos_counter():
	var current_tomatos:int = TomatoLogic.count_tomatos()
	$top_ui/tomato_counter.text = str(current_tomatos)
